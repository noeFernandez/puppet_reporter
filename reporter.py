import json
import urllib2, base64


class Host:
    def __init__(self, computer_name):
        self.computer_name = computer_name
        self.ram = None
        self.processor = None
        self.model = None
        self.os = None
        self.lab = None


def get_all_hosts():
    hosts = []

    hostsJSON = signed_hhtp_get_petition("https://youpuppetip/api/v2/hosts?per_page=10000")
    for host in hostsJSON["results"]:
        hosts.append(Host(host["name"]))

    for host in hosts:
        host_facts_json = signed_hhtp_get_petition(
            "https://youpuppetip/api/v2/hosts/{}/facts?per_page=1000000".format(host.computer_name))
        try:
            host.processor = host_facts_json["results"][host.computer_name]["processor0"]
            host.ram = host_facts_json["results"][host.computer_name]["memorysize"]
            host.model = host_facts_json["results"][host.computer_name]["productname"]
            host.os = host_facts_json["results"][host.computer_name]["osfamily"] +" "+ \
                      host_facts_json["results"][host.computer_name]["operatingsystemrelease"]

        except KeyError:
            pass

    return hosts


def signed_hhtp_get_petition(url):
    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % ("user", "password")).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)
    result = urllib2.urlopen(request)
    return json.load(result.fp)


hosts = get_all_hosts()

for host in hosts:
    try:
        lab = None
        if host.computer_name.startswith("eii-l-s"):
            lab = "L-S-"  + host.computer_name.split("-")[3]
        elif host.computer_name.startswith("eii-l"):
            lab = "L-"  + host.computer_name.split("-")[2]

        if lab != None:
            print "{}\t{}\t{}\t{}\t{}\t{}".format(host.ram,lab,host.computer_name,host.processor, host.model, host.os)
    except AttributeError, IndexError:
        pass

